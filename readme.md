### homebridge-hive-forever

This [HomeBridge](https://github.com/nfarina/homebridge) Plugin adds support for all your hive devices. This is the version deprecates the old modules that were installed separately.  


#### About

Homebridge Hive Forever was created to be used in conjunction with your Homebridge setup. After setting up this plugin correctly you will be able to interact with your devices via the Apple Home app and via Siri voice control.


#### Installation

**1. Install HomeBridge**

Details on the installation process can be found here [HomeBridge](https://github.com/nfarina/homebridge).

**2. Installing this Plugin**

Run this command to download homebridge-hive-forever. This includes the entire collection. You do not need to own all the devices in order to create a successful setup, you can pick the products that you want connected.

```
npm install -g homebridge-hive-forever
```

**3. Configure HomeBridge**

In your homebridge `config.json` file, add this accessory providing the username and password you use to log into https://my.hivehome.com. The following example will show you how to connect smart plug. Please look below to see a list of supported devices and replace the *"product"* value with the device that you want to connect.

```
"accessories": [
    {
        "accessory": "HiveForever",
        "product": "HiveSmartPlug",
        "name": "Plug 1",
        "displayName": "The name that you want to display on the Home app and communicate via Siri",
        "username": "YOUR_USERNAME",
        "password": "SECRET"
​    }
],
```

***Note:** Please ensure that the name matches the name you have given to the device. For example if you have renamed your Light to John's Smart Plug, then you will need to replace "Plug 1" with "John's Smart Plug", the name is case-sensitive so please make sure you enter it correctly.*

**4. Configuring Multiple Thermostats**

If you wish to configure multiple thermostats; you must include the device id in the config object. If you don't know the device id, run the HiveThermostat config without an identifier. You will see the device id's for all thermostats printed into the Terminal window.

Due to limitation of the HIVE api, we currently can't switch between different heating states such as schedule and off, therefore the plugin will always report the thermostat under "heat" mode but this should be read as if it's currently under schedule mode.

**Single**

Note how we don't include the "id" key.

```
"accessories": [
    {
        "accessory": "HiveForever",
        "product": "HiveThermostat",
        "name": "This value doesn\'t matter",
        "displayName": "The name that you want to display on the Home app and communicate via Siri",
        "username": "YOUR_USERNAME",
        "password": "SECRET"
​    },
],
```

**Multiple** 

Note how we include a unique id for each thermostat.

```
"accessories": [
    {
        "accessory": "HiveForever",
        "product": "HiveThermostat",
        "name": "This value doesn\'t matter",
        "id": "11111111-1111-1111-1111-111111111111"
        "displayName": "The name that you want to display on the Home app and communicate via Siri",
        "username": "YOUR_USERNAME",
        "password": "SECRET"
​    },
    {
        "accessory": "HiveForever",
        "product": "HiveThermostat",
        "name": "This value doesn\'t matter",
        "id": "22222222-2222-2222-2222-222222222222"
        "displayName": "The other thermostat",
        "username": "YOUR_USERNAME",
        "password": "SECRET"
    }
],
```



**5. Restart HomeBridge**

Restart homebridge and you should see all the devices that you have connected.



#### Supported Devices

Use the provided instructions but replace the "*product*" value with one of the following to setup the device.

Dimmable Light: **Hive Light**

Active / Tunable Light: **HiveTunableLight**

SmartPlug: **HiveSmartPlug**

Thermostat:  **HiveThermostat**



#### FAQ

**Q. Why does the name have to match the name on the Hive Dashboard?**
This is to allow multiple instances of the same accessory, for example you may want to connect multiple light bulbs.

**Q. You do not support a device, can you add it?**
Yes, please contact me using the email address provided below.

**Q. I'm having problems connecting my device, can you fix it?**
Sure, please contact me but I can't be sure that I can fix it right away.

**Q. What will happen to the old modules?**
Maintaining them separately was getting tedious, a combined module will let me update the code faster and I do not need to roll updates across all plugins when I need to update multiple.

**Q. What happened to the Hot Water plugin?**
I am currently working on re-implementing support for hot water as it wasn't working as expected.



#### Contact Me

You can reach me here: winson.hero@gmail.com