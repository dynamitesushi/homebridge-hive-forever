"use strict";

var q = [];
var Service, Characteristic;
var request = require("request");

module.exports = homebridge => {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory(
    "homebridge-hive-forever",
    "HiveForever",
    HiveForever
  );
};

/**
 * Valid products and their hive schema identifiers
 */
var products = {
  hivesmartplug: "http://alertme.com/schema/json/node.class.smartplug.json#",
  hivelight: "http://alertme.com/schema/json/node.class.light.json#",
  hivetunablelight:
    "http://alertme.com/schema/json/node.class.tunable.light.json#",
  hivethermostat: "http://alertme.com/schema/json/node.class.thermostat.json#",
  hivethermostatboost:
    "http://alertme.com/schema/json/node.class.thermostat.json#"
};

/**
 * Create a list of valid products
 */
const validProducts = ["debug"];
for (var key in products) {
  validProducts.push(key);
}

/**
 * Let's go baby!
 */
class HiveForever {
  /**
   * Const
   * @param {*} log
   * @param {*} config
   */
  constructor(log, config) {
    this.log = log;

    this.node = null;
    this.sessionId = null;

    /**
     * Hive configuration
     */
    this.baseHive = "https://api-prod.bgchprod.info:443/omnia";
    this.username = config.username;
    this.password = config.password;
    this.initialised = false;

    /**
     * Accessory configuration
     */
    this.name = config.name;
    this.displayName = config.displayName;
    this.product = config.product ? config.product.toLowerCase() : "unknown";
    this.debug = config.debug || false;
    this.debugDevice = config.debugDevice || "";

    /**
     * Validate product
     */
    if (!this.product || !validProducts.includes(this.product)) {
      this.log(
        "Please enter a valid hive product that is supported by Hive Forever"
      );
    }

    /**
     * Validate name
     */
    if (!this.name) {
      this.log(
        "Please provide the name of the hive product that you have assigned to the product. You can find this on your Hive dashboard, this is needed to" +
          " support multiple instances of the same accessory."
      );
    }

    /**
     * Configure Service
     */
    switch (this.product) {
      case "hivesmartplug":
        this.service = new Service.Switch(this.displayName);
        break;
      case "hivetunablelight":
        this.service = new Service.Lightbulb(this.displayName);
        break;
      case "hivelight":
        this.service = new Service.Lightbulb(this.displayName);
        break;
      case "hivethermostat":
        this.service = new Service.Thermostat(this.displayName);
        break;
      case "hivethermostatboost":
        this.service = new Service.Switch(this.displayName);
        break;
      case "debug":
        this.service = new Service.Switch("Hive Forever Debug Mode");
    }

    var me = this;
    setInterval(function() {
      me.queue();
    }, 1000);
  }

  /**
   * Get product name
   */
  getProductName() {
    switch (this.product) {
      default:
        return "N/A";
    }
  }

  /**
   * Get Services
   */
  getServices() {
    var informationService = new Service.AccessoryInformation()
      .setCharacteristic(Characteristic.Manufacturer, "British Gas")
      .setCharacteristic(Characteristic.Model, this.getProductName())
      .setCharacteristic(Characteristic.SerialNumber, "N/A");

    /**
     * Configure Product Services
     */
    switch (this.product) {
      case "hivesmartplug":
        this.service
          .getCharacteristic(Characteristic.On)
          .on("get", this.getSmartPlugStatus.bind(this))
          .on("set", this.setSmartPlugStatus.bind(this));
        break;
      case "hivetunablelight":
        this.service
          .getCharacteristic(Characteristic.On)
          .on("get", this.getTunableLightOnCharacteristic.bind(this))
          .on("set", this.setTunableLightOnCharacteristic.bind(this));
        this.service
          .getCharacteristic(Characteristic.Brightness)
          .on("get", this.getTunableLightBrightnessCharacteristic.bind(this))
          .on("set", this.setTunableLightBrightnessCharacteristic.bind(this));
        break;
      case "hivelight":
        this.service
          .getCharacteristic(Characteristic.On)
          .on("get", this.getLightOnCharacteristic.bind(this))
          .on("set", this.setLightOnCharacteristic.bind(this));
        this.service
          .getCharacteristic(Characteristic.Brightness)
          .on("get", this.getLightBrightnessCharacteristic.bind(this))
          .on("set", this.setLightBrightnessCharacteristic.bind(this));
        break;
      case "hivethermostatboost":
        this.service
          .getCharacteristic(Characteristic.On)
          .on("get", this.getThermostatBoostState.bind(this))
          .on("set", this.setThermostatBoostState.bind(this));
        break;
      case "hivethermostat":
        this.service
          .getCharacteristic(Characteristic.CurrentTemperature)
          .on(
            "get",
            this.getThermostatCurrentTempatureCharacteristic.bind(this)
          );
        this.service
          .getCharacteristic(Characteristic.CurrentHeatingCoolingState)
          .on(
            "get",
            this.getThermostatCurrentHeatingCoolingStateCharacteristic.bind(
              this
            )
          );
        this.service
          .getCharacteristic(Characteristic.TargetHeatingCoolingState)
          .on(
            "get",
            this.getThermostatCurrentHeatingCoolingStateCharacteristic.bind(
              this
            )
          )
          .on(
            "set",
            this.setThermostatCurrentHeatingCoolingStateCharacteristic.bind(
              this
            )
          );
        this.service
          .getCharacteristic(Characteristic.TargetTemperature)
          .setProps({
            minValue: 5.0,
            maxValue: 32.0,
            minStep: 0.5
          })
          .on(
            "get",
            this.getThermostatTargetTemperatureCharacteristic.bind(this)
          )
          .on(
            "set",
            this.setThermostatTargetTemperatureCharacteristic.bind(this)
          );
        break;
      case "debug":
        this.service
          .getCharacteristic(Characteristic.On)
          .on("get", this.getDebug.bind(this));
        break;
    }

    return [informationService, this.service];
  }

  /**
   * Login to Hive
   * -- Login to the Hive service
   */
  loginToHive(callback) {
    var me = this;
    request.post(
      {
        url: this.baseHive + "/auth/sessions",
        headers: {
          "Content-Type": "application/vnd.alertme.zoo-6.1+json",
          Accept: "application/vnd.alertme.zoo-6.1+json",
          "X-Omnia-Client": "Hive Web Dashboard"
        },
        body: JSON.stringify({
          sessions: [
            {
              username: this.username,
              password: this.password,
              caller: "WEB"
            }
          ]
        })
      },
      function(error, response, body) {
        var resp = JSON.parse(body);
        if (resp.errors) {
          if (me.debug && !me.initialised) {
            me.log(body);
            me.initialised = true;
          }
          me.log(
            "Please ensure your credentials to log into your Hive account is correct."
          );
        } else {
          me.sessionId = resp.sessions[0].sessionId;
          me.getProduct(callback);
        }
      }.bind(this)
    );
  }

  /**
   * Get Product
   */
  getProduct(callback) {
    var me = this;
    request.get(
      {
        url: this.baseHive + "/nodes",
        headers: {
          "Content-Type": "application/vnd.alertme.zoo-6.1+json",
          Accept: "application/vnd.alertme.zoo-6.1+json",
          "X-Omnia-Client": "Hive Web Dashboard",
          "X-Omnia-Access-Token": this.sessionId
        }
      },
      function(error, response, body) {
        var resp = JSON.parse(body);
        if (resp.errors) {
          me.log("You could not login to your hive account.");
        } else {
          if (me.debug || me.product === "debug") {
            var deviceNames = [];
            resp.nodes.forEach(function(node) {
              if (node.attributes.nodeType) {
                deviceNames.push(node.name);
              }
            });
            me.log(
              "------------------------------------------------------------"
            );
            me.log("[DEBUG] - List of Connected Devices");
            me.log(
              "------------------------------------------------------------"
            );
            me.log(deviceNames);
            me.log(
              "------------------------------------------------------------"
            );
          }

          if (me.debugDevice) {
            var attributes = "";
            resp.nodes.forEach(function(node) {
              if (
                node.attributes.nodeType &&
                node.attributes.nodeType.displayValue === me.debugDevice
              ) {
                attributes = node.attributes;
              }
            });
            me.log(
              "------------------------------------------------------------"
            );
            me.log("[DEBUG] - Device Attributes");
            me.log(
              "------------------------------------------------------------"
            );
            me.log(attributes);
            me.log(
              "------------------------------------------------------------"
            );
          }

          /**
           * Set node id so we can interact with device
           */
          resp.nodes.forEach(function(node) {
            if (
              node.attributes.nodeType &&
              node.attributes.nodeType.displayValue === products[me.product] &&
              node.name === me.name &&
              (me.product !== "hivethermostat" ||
                me.product !== "hivethermostatboost")
            ) {
              me.node = node.id;
            }

            // Set thermostat to the correct instance since multiple are returned
            if (
              (me.product === "hivethermostat" ||
                me.product === "hivethermostatboost") &&
              node.attributes.hasOwnProperty("temperature") &&
              node.attributes.nodeType.reportedValue === products[me.product]
            ) {
              if (!me.initialised) {
                me.log(
                  'Found Thermostat: "' +
                    node.id +
                    '". If you have wish to configure multiple thermostats, please include the id of the thermostat in your configuration file with the key "id".'
                );
                me.initialised = true;
              }

              if (me.hasOwnProperty("id") && me.id === node.id) {
                return (me.node = node.id);
              }
              me.node = node.id;
            }
          });

          /**
           * If no node id was found, let's show an error to the user
           */
          if (!me.node && !me.debug && me.product !== "debug") {
            me.log(
              "Please ensure that the device that you're connecting has the same name that shows up on the Hive dashboard."
            );
          }

          if (!me.debug && me.product !== "debug") {
            return callback();
          }
        }
      }.bind(this)
    );
  }

  /**
   * Base Request
   * -- Used for interacting with the bulb
   */
  baseRequest(type, body, callback) {
    var me = this;
    request[type](
      {
        url: this.baseHive + "/nodes/" + this.node,
        headers: {
          "Content-Type": "application/vnd.alertme.zoo-6.1+json",
          Accept: "application/vnd.alertme.zoo-6.1+json",
          "X-Omnia-Client": "Hive Web Dashboard",
          "X-Omnia-Access-Token": this.sessionId
        },
        body: JSON.stringify(body)
      },
      function(error, response, body) {
        var responseJSON = JSON.parse(body);
        if (responseJSON.errors) {
          return me.loginToHive(function() {
            me.baseRequest(type, body, callback);
          });
        }
        return callback(responseJSON);
      }.bind(this)
    );
  }

  /**
   * SmartPlug
   */
  getSmartPlugStatus(next) {
    var me = this;
    me.baseRequest("get", null, function(response) {
      return next(
        null,
        response.nodes[0].attributes.state.displayValue === "ON"
      );
    });
  }
  setSmartPlugStatus(on, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                state: {
                  targetValue: on === true ? "ON" : "OFF"
                }
              }
            }
          ]
        },
        function(response) {
          return next();
        }
      );
    });
  }

  /**
   * Light
   */
  getLightOnCharacteristic(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        const device = response.nodes[0].attributes.state;
        return next(null, device ? device.reportedValue === "ON" : false);
      });
    });
  }
  setLightOnCharacteristic(on, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                state: {
                  targetValue: on === true ? "ON" : "OFF"
                }
              }
            }
          ]
        },
        function(response) {
          return next();
        }
      );
    });
  }
  getLightBrightnessCharacteristic(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        const device = response.nodes[0].attributes.state;
        return next(null, device ? device.reportedValue === "ON" : 0);
      });
    });
  }
  setLightBrightnessCharacteristic(brightness, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                brightness: {
                  targetValue: brightness
                }
              }
            }
          ]
        },
        function(response) {
          return next();
        }
      );
    });
  }

  /**
   * TunableLight
   */
  getTunableLightOnCharacteristic(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        return next(
          null,
          response.nodes[0].attributes.state.reportedValue === "ON"
        );
      });
    });
  }
  setTunableLightOnCharacteristic(on, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                state: {
                  targetValue: on === true ? "ON" : "OFF"
                }
              }
            }
          ]
        },
        function() {
          return next();
        }
      );
    });
  }
  getTunableLightBrightnessCharacteristic(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        return next(
          null,
          response.nodes[0].attributes.brightness.reportedValue
        );
      });
    });
  }
  setTunableLightBrightnessCharacteristic(brightness, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                brightness: {
                  targetValue: brightness
                }
              }
            }
          ]
        },
        function(response) {
          return next();
        }
      );
    });
  }

  /**
   * Thermostat Boost
   */
  getThermostatBoostState(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        return next(
          null,
          response.nodes[0].attributes.activeHeatCoolMode === "BOOST"
        );
      });
    });
  }
  setThermostatBoostState(on, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                activeHeatCoolMode: {
                  targetValue: "BOOST"
                },
                scheduleLockDuration: {
                  targetValue: on ? 60 : 0
                },
                targetHeatTemperature: {
                  targetValue: 32
                }
              }
            }
          ]
        },
        function(response) {
          return next(
            null,
            response.nodes[0].attributes.activeHeatCoolMode === "BOOST"
          );
        }
      );
    });
  }

  /**
   * Thermostat
   */
  getThermostatCurrentTempatureCharacteristic(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        return next(
          null,
          response.nodes[0].attributes.temperature.reportedValue
        );
      });
    });
  }
  getThermostatCurrentHeatingCoolingStateCharacteristic(next) {
    var me = this;
    return next(null, 1);
  }
  setThermostatCurrentHeatingCoolingStateCharacteristic(value, next) {
    var me = this;
    return next(null, 1);
  }
  getThermostatTargetTemperatureCharacteristic(next) {
    var me = this;
    q.push(function() {
      me.baseRequest("get", null, function(response) {
        return next(
          null,
          response.nodes[0].attributes.targetHeatTemperature.reportedValue
        );
      });
    });
  }
  setThermostatTargetTemperatureCharacteristic(temp, next) {
    var me = this;
    q.push(function() {
      me.baseRequest(
        "put",
        {
          nodes: [
            {
              attributes: {
                activeHeatCoolMode: {
                  targetValue: "HEAT"
                },
                targetHeatTemperature: {
                  targetValue: temp
                }
              }
            }
          ]
        },
        function(response) {
          return next();
        }
      );
    });
  }

  /**
   * Debug
   */
  getDebug(next) {
    var me = this;
    me.baseRequest("get", null, null);
    return next(null, true);
  }

  /**
   * Queue Handler
   */
  queue() {
    if (q.length !== 0) {
      q.shift()();
    }
  }
}
